@extends('layouts.app')

@push('css')
    <link href='{{asset("css/style.css")}}' rel="stylesheet">
@endpush



@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Chat</div>

                <div class="card-body">

                    <div id="chat-sidebar">
                        @foreach(App\User::all() as $user)
                          <div id="sidebar-user-box" class="user" uid="{{ $user->id }}" >
                            <img src='{{ asset("image/user.png" ) }}'/>
                            <span id="slider-username">{{ $user->name }}</span>
                            <span class="user_status user_{{$user->id}}">&nbsp;</span>
                        </div>
                        @endforeach
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-1.10.1.min.js') }}"></script>
    <script type="text/javascript">
        let userid = '{{ auth()->id() }}';
        let username = '{{ auth()->user()->name }}';
    </script>
    <script src="{{ asset('js/script.js') }}"></script>
@endpush