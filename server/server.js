'use strict';

const express  = require('express');
const http     = require('http');
const socket   = require('socket.io');
const SocketServer = require('./socket');


class Server {
    constructor()
    {
        this.port = 9000;
        this.host = 'localhost';

        this.app = express();
        this.http = http.Server(this.app);
        this.scoket = socket(this.http);
    }

    runServer()
    {
        new SocketServer(this.scoket).socketConnection();

        this.http.listen(this.port,this,this.host , () =>{
             console.log(`server is running at ${this.host}`)
        })
    }
}

const app = new Server();
app.runServer();




