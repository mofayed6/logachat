'use strict';

class Socket {
    constructor(socket)
    {
        this.io = socket;
        this.online_user = [];
    }

    ioConfig()
    {
        this.io.use((socket,next)=>{
            socket['id'] = 'user_'+ socket.handshake.query.user_id;
            next();
         });
    }

    check_online(socket)
    {
        socket.on('check_online',(data) => {
            if(this.online_user.indexOf(data.user_id) != -1)
            {
                var status = 'online';
                this.io.sockets.connected[data.user_id].emit('new_online',{
                    user_id:socket.id,
                    status:'online'
                });
            }else {
                var status = 'offline';

            }

            this.io.sockets.connected[socket.id].emit('is_online',{
                user_id:data.user_id,
                status:status
            });


        });
    }

    socketConnection()
    {
        this.ioConfig();
        this.io.on('connection',(socket) => {
            this.online_user = Object.keys(this.io.sockets.sockets);
            this.check_online(socket);
            this.socketDisconnect(socket);

        });

    }

    socketDisconnect(socket)
    {
        socket.on('disconnect',(data)=> {

        });
    }
}

module.exports = Socket;